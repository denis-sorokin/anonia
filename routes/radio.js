const express = require('express');
const router = express.Router();

const RadioController = require('../controller/radio');

/* GET radio room */
router.get('/room/:id', async function(req, res) {
		RadioController.radioListen(req, res);
	}
);

module.exports = router;
