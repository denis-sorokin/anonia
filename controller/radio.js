const fs = require('fs');

class RadioController {
	constructor() {
	}

	radioListen(req, res) {
		console.log(`Send audio from room #${req.params.id}`);

		const filePath = 'test.mp3';
		const stat = fs.statSync(filePath);

		res.writeHead(200, {
			'Content-Type': 'audio/mpeg',
			'Content-Length': stat.size
		});

		const readStream = fs.createReadStream(filePath);
		readStream.pipe(res);
	}
}

module.exports = new RadioController();
