require('dotenv').config({path: './.env'});
const express = require('express');
const fs = require('fs');
const errorHandler = require('./errorHandler');

const RadioRouter = require('./routes/radio');

const app = express();

const io = require('socket.io')(process.env.SOCKET || 11375);

const port = process.env.PORT || 3005;

app.all('*', function(req,res,next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With");
	res.header("Access-Control-Allow-Methods", "GET, PUT, POST, OPTIONS");
	next();
});

io.on('connection', function (socket) {
	socket.on('stream', function (obj) {
		const { audio, room } = obj;
		if (!fs.existsSync(room)) {
			fs.mkdir(room);
		}
		const stream = fs.createReadStream(audio);

		// TODO create every hour new file
		const writeStream = fs.createWriteStream(`${room}/stream.js`);
		stream.pipe(writeStream, { end: false })
	});

	socket.on('pingServer', function (ping) {
		console.log(ping);
		socket.emit('pIng', 'pong');
	})
});

app.use('/radio', RadioRouter);

app.use(errorHandler.init);

app.listen(port, (err) => {
	if (err) {
		return console.log('Error start server', err);
	}

	console.log(`Server start on ${port} port.`);
});
