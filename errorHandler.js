class ErrorHandler {
	constructor (error, request, response, next) {
		this.err = error,
		this.req = request,
		this.res = response,
		this.next = next
	}

	init () {
		if (!this.err.status) this.err.status = 500;
		console.log(this.err);
		this.res.send(this.err, this.err.status);
	}
}

module.exports = new ErrorHandler();
